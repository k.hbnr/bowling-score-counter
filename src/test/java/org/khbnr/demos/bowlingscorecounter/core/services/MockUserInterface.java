package org.khbnr.demos.bowlingscorecounter.core.services;

import java.util.Arrays;
import java.util.List;

public class MockUserInterface implements UserInterface {

    private static final List<Integer> mockReturnValues = Arrays.asList(1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 2, 8, 6);
    private int index = 0;

    @Override
    public int getScoreForRoll(final int frameNumber, final int rollNumber) {
        if (index > MockUserInterface.mockReturnValues.size()) {
            throw new IllegalArgumentException("Too many requests on mock!");
        }

        final int mockValue = MockUserInterface.mockReturnValues.get(index);
        index++;
        return mockValue;
    }

    @Override
    public void printErrorMessage(String errorMessage) {

    }

    @Override
    public void printResult(int finalScore) {

    }

}
