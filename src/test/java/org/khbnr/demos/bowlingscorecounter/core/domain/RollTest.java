package org.khbnr.demos.bowlingscorecounter.core.domain;

import static org.assertj.core.api.Java6Assertions.assertThat;

import org.junit.jupiter.api.Test;

class RollTest {

    private static final int LEFT_NUMBER_OF_PINS = 10;

    private Roll rollUnderTest;

    @Test
    void getScore_should_return_5_without_bonus() {
        final int expected = 5;

        rollUnderTest = new Roll(5, RollTest.LEFT_NUMBER_OF_PINS, true, false);
        final int actual = rollUnderTest.getScoreWithBonus();

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void getScore_should_return_10_with_bonus() {
        final int expected = 10;

        rollUnderTest = new Roll(5, RollTest.LEFT_NUMBER_OF_PINS, true, true);
        final int actual = rollUnderTest.getScoreWithBonus();

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void isStrike_should_return_true() {
        rollUnderTest = new Roll(RollTest.LEFT_NUMBER_OF_PINS, RollTest.LEFT_NUMBER_OF_PINS, true, true);

        assertThat(rollUnderTest.isStrike()).isTrue();
    }

    @Test
    void isStrike_should_return_false_when_it_is_too_less_pins() {
        rollUnderTest = new Roll(5, RollTest.LEFT_NUMBER_OF_PINS, true, true);

        assertThat(rollUnderTest.isStrike()).isFalse();
    }

    @Test
    void isStrike_should_return_false_when_it_is_not_the_first_roll_of_a_frame() {
        rollUnderTest = new Roll(RollTest.LEFT_NUMBER_OF_PINS, RollTest.LEFT_NUMBER_OF_PINS, false, true);

        assertThat(rollUnderTest.isStrike()).isFalse();
    }

    @Test
    void isSpareOrStrike_should_return_true_if_it_is_a_strike() {
        rollUnderTest = new Roll(RollTest.LEFT_NUMBER_OF_PINS, RollTest.LEFT_NUMBER_OF_PINS, true, true);

        assertThat(rollUnderTest.isSpareOrStrike()).isTrue();
    }

    @Test
    void isSpareOrStrike_should_return_true_if_it_is_a_spare() {
        rollUnderTest = new Roll(RollTest.LEFT_NUMBER_OF_PINS, RollTest.LEFT_NUMBER_OF_PINS, false, true);

        assertThat(rollUnderTest.isSpareOrStrike()).isTrue();
    }

    @Test
    void isSpareOrStrike_should_return_false_when_it_is_too_less_pins() {
        rollUnderTest = new Roll(5, RollTest.LEFT_NUMBER_OF_PINS, true, true);

        assertThat(rollUnderTest.isSpareOrStrike()).isFalse();
    }


}