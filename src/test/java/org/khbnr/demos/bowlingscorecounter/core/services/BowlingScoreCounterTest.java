package org.khbnr.demos.bowlingscorecounter.core.services;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.khbnr.demos.bowlingscorecounter.core.services.BowlingScoreCounter.BonusType;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BowlingScoreCounterTest {

    private static final int TWO_FRAMES = 2;
    private static final int TEN_FRAMES = 10;
    private static final int INITIAL_SCORE = 0;
    private static final int INITIAL_FRAME_NUMBER = 1;

    @Mock
    private UserInterface userInterface;

    private BowlingScoreCounter bowlingScoreCounterUnderTest;

    @BeforeEach
    void setUp() {
        bowlingScoreCounterUnderTest = new BowlingScoreCounter(userInterface, BowlingScoreCounterTest.TWO_FRAMES);
    }

    @Test
    void playGame_should_return_same_result_as_example_game_on_task_description() {
        final int expected = 133;

        bowlingScoreCounterUnderTest = new BowlingScoreCounter(new MockUserInterface(), BowlingScoreCounterTest.TEN_FRAMES);
        final int actual = bowlingScoreCounterUnderTest.playGame();

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void playFrame_should_return_16() {
        final int expected = 16;
        doReturn(4, 3, 2, 7).when(userInterface).getScoreForRoll(anyInt(), anyInt());

        final int result = bowlingScoreCounterUnderTest
            .playFrame(BowlingScoreCounterTest.INITIAL_SCORE, BowlingScoreCounterTest.INITIAL_FRAME_NUMBER, BonusType.NO_BONUS);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void playFrame_should_apply_strike_bonus() {
        final int expected = 24;
        doReturn(10, 4, 3).when(userInterface).getScoreForRoll(anyInt(), anyInt());

        final int result = bowlingScoreCounterUnderTest
            .playFrame(BowlingScoreCounterTest.INITIAL_SCORE, BowlingScoreCounterTest.INITIAL_FRAME_NUMBER, BonusType.NO_BONUS);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void playFrame_should_apply_spare_bonus() {
        final int expected = 21;
        doReturn(5, 5, 4, 3).when(userInterface).getScoreForRoll(anyInt(), anyInt());

        final int result = bowlingScoreCounterUnderTest
            .playFrame(BowlingScoreCounterTest.INITIAL_SCORE, BowlingScoreCounterTest.INITIAL_FRAME_NUMBER, BonusType.NO_BONUS);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void playFrame_should_return_35_due_to_last_frame_bonus() {
        final int expected = 35;
        doReturn(10, 10, 5).when(userInterface).getScoreForRoll(anyInt(), anyInt());

        final int result = bowlingScoreCounterUnderTest
            .playFrame(BowlingScoreCounterTest.INITIAL_SCORE, BowlingScoreCounterTest.INITIAL_FRAME_NUMBER, BonusType.NO_BONUS);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void playFrame_should_return_0() {
        final int expected = 0;
        doReturn(0).when(userInterface).getScoreForRoll(anyInt(), anyInt());

        final int result = bowlingScoreCounterUnderTest
            .playFrame(BowlingScoreCounterTest.INITIAL_SCORE, BowlingScoreCounterTest.INITIAL_FRAME_NUMBER, BonusType.NO_BONUS);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void playFrame_should_throw_IllegalArgumentException_when_score_is_smaller_than_0() {
        doReturn(-1).when(userInterface).getScoreForRoll(anyInt(), anyInt());

        assertThrows(IllegalArgumentException.class,
            () -> bowlingScoreCounterUnderTest
                .playFrame(BowlingScoreCounterTest.INITIAL_SCORE, BowlingScoreCounterTest.INITIAL_FRAME_NUMBER, BonusType.NO_BONUS));
    }

    @Test
    void playFrame_should_throw_IllegalArgumentException_when_score_is_greather_than_10() {
        doReturn(11).when(userInterface).getScoreForRoll(anyInt(), anyInt());

        assertThrows(IllegalArgumentException.class,
            () -> bowlingScoreCounterUnderTest
                .playFrame(BowlingScoreCounterTest.INITIAL_SCORE, BowlingScoreCounterTest.INITIAL_FRAME_NUMBER, BonusType.NO_BONUS));
    }

    @Test
    void playFrame_should_throw_IllegalArgumentException_when_score_is_greather_than_10_in_two_rolls() {
        doReturn(5, 6).when(userInterface).getScoreForRoll(anyInt(), anyInt());

        assertThrows(IllegalArgumentException.class,
            () -> bowlingScoreCounterUnderTest
                .playFrame(BowlingScoreCounterTest.INITIAL_SCORE, BowlingScoreCounterTest.INITIAL_FRAME_NUMBER, BonusType.NO_BONUS));
    }
}