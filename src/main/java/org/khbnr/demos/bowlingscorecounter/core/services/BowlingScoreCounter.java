package org.khbnr.demos.bowlingscorecounter.core.services;

import java.util.Arrays;
import java.util.Objects;
import org.khbnr.demos.bowlingscorecounter.core.domain.Roll;

public class BowlingScoreCounter {

    private static final int MAX_NUMBER_OF_PINS = 10;
    private static final int INITIAL_SCORE = 0;
    private static final int INITIAL_FRAME_NUMBER = 1;

    private final UserInterface userInterface;
    private final int numberOfFramesInGame;

    public BowlingScoreCounter(final UserInterface userInterface, final int numberOfFramesInGame) {
        this.userInterface = userInterface;
        this.numberOfFramesInGame = numberOfFramesInGame;
    }

    public int playGame() {
        return playFrame(BowlingScoreCounter.INITIAL_SCORE, BowlingScoreCounter.INITIAL_FRAME_NUMBER, BonusType.NO_BONUS);
    }

    protected int playFrame(final int currentScore, final int frameNumber, final BonusType bonusTypeFromPreviousFrame) {

        final Roll firstRoll = playFirstRoll(frameNumber, bonusTypeFromPreviousFrame);

        if (firstRoll.isStrike()) {
            return handleFrameResult(currentScore, frameNumber, firstRoll);
        }

        final Roll secondRoll = playSecondRoll(frameNumber, firstRoll.getScoreWithoutBonus(), bonusTypeFromPreviousFrame);

        return handleFrameResult(currentScore, frameNumber, firstRoll, secondRoll);
    }

    private int handleFrameResult(final int currentScore, final int frameNumber, final Roll firstRoll) {
        return isLastFrame(frameNumber) ? handleLastFrame(frameNumber, currentScore, firstRoll, null)
            : playFrame(computeScore(currentScore, firstRoll), frameNumber + 1, BonusType.STRIKE_BONUS);
    }

    private int handleFrameResult(final int currentScore, final int frameNumber, final Roll firstRoll, final Roll secondRoll) {
        if (isLastFrame(frameNumber)) {
            return handleLastFrame(frameNumber, currentScore, firstRoll, secondRoll);
        }

        final int newScore = computeScore(currentScore, firstRoll, secondRoll);

        final BonusType bonusTypeForThisFrame = secondRoll.isSpareOrStrike() ? BonusType.SPARE_BONUS : BonusType.NO_BONUS;
        return playFrame(newScore, frameNumber + 1, bonusTypeForThisFrame);
    }

    private int handleLastFrame(final int frameNumber, final int currentScore, final Roll firstRoll, final Roll secondRoll) {
        final Roll extraRoll = hasExtraRoll(firstRoll, secondRoll) ? playExtraRoll(frameNumber) : null;

        return computeScore(currentScore, firstRoll, secondRoll, extraRoll);
    }

    private Roll playFirstRoll(final int frameNumber, final BonusType bonusTypeFromPreviousFrame) {
        final int score = userInterface.getScoreForRoll(frameNumber, 1);
        final boolean hasBonus = bonusTypeIsSpareOrStrike(bonusTypeFromPreviousFrame);
        try {
            return new Roll(score, BowlingScoreCounter.MAX_NUMBER_OF_PINS, true, hasBonus);
        } catch (final IllegalArgumentException e) {
            userInterface.printErrorMessage(e.getMessage());
            throw e; // should rather implement retry
        }
    }

    private Roll playSecondRoll(final int frameNumber, final int scoredPinsFromPreviousRoll, final BonusType bonusTypeFromPreviousFrame) {
        final int leftPins = BowlingScoreCounter.MAX_NUMBER_OF_PINS - scoredPinsFromPreviousRoll;
        final int score = userInterface.getScoreForRoll(frameNumber, 2);
        final boolean hasBonus = bonusTypeIsStrike(bonusTypeFromPreviousFrame);
        try {

            return new Roll(score, leftPins, false, hasBonus);
        } catch (final IllegalArgumentException e) {
            userInterface.printErrorMessage(e.getMessage());
            throw e; // should rather implement retry
        }
    }

    private Roll playExtraRoll(final int frameNumber) {
        try {
            return new Roll(userInterface.getScoreForRoll(frameNumber, 3), BowlingScoreCounter.MAX_NUMBER_OF_PINS, false, false);
        } catch (final IllegalArgumentException e) {
            userInterface.printErrorMessage(e.getMessage());
            throw e; // should rather implement retry
        }
    }

    private boolean hasExtraRoll(final Roll firstRoll, final Roll secondRoll) {
        return firstRoll.isStrike() || secondRoll.isSpareOrStrike();
    }

    private boolean isLastFrame(final int frameNumber) {
        return frameNumber == numberOfFramesInGame;
    }

    private int computeScore(final int currentScore, final Roll... rolls) {
        return currentScore + Arrays.stream(rolls)
            .filter(Objects::nonNull)
            .mapToInt(Roll::getScoreWithBonus)
            .sum();
    }

    private boolean bonusTypeIsSpareOrStrike(final BonusType bonusType) {
        return bonusType.equals(BonusType.SPARE_BONUS) || bonusType.equals(BonusType.STRIKE_BONUS);
    }

    private boolean bonusTypeIsStrike(final BonusType bonusType) {
        return bonusType.equals(BonusType.STRIKE_BONUS);
    }

    protected enum BonusType {
        NO_BONUS,
        SPARE_BONUS,
        STRIKE_BONUS
    }
}
