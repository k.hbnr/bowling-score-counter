package org.khbnr.demos.bowlingscorecounter.core.services;

public interface UserInterface {

    int getScoreForRoll(final int frameNumber, final int rollNumber);

    void printErrorMessage(final String errorMessage);

    void printResult(final int finalScore);

}
