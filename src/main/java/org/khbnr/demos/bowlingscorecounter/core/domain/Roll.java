package org.khbnr.demos.bowlingscorecounter.core.domain;

public class Roll {

    private static final int BONUS_FACTOR = 2;
    private static final int MIN_NUMBER_OF_PINS = 0;

    final int score;
    final int leftNumberOfPins;
    final boolean isFirstRollOfFrame;
    final boolean hasBonusFromPreviousFrame;

    public Roll(final int score, final int leftNumberOfPins, final boolean isFirstRollOfFrame, final boolean hasBonusFromPreviousFrame) {
        ValidateScore(score, leftNumberOfPins);
        this.score = score;
        this.leftNumberOfPins = leftNumberOfPins;
        this.isFirstRollOfFrame = isFirstRollOfFrame;
        this.hasBonusFromPreviousFrame = hasBonusFromPreviousFrame;
    }

    public int getScoreWithoutBonus() {
        return score;
    }

    public int getScoreWithBonus() {
        return hasBonusFromPreviousFrame ? score * Roll.BONUS_FACTOR : score;
    }

    public boolean isStrike() {
        return isFirstRollOfFrame && isSpareOrStrike(); //due to wikipedia it is only spike if it's the first roll of a frame
    }

    public boolean isSpareOrStrike() {
        return leftNumberOfPins == score;
    }

    private void ValidateScore(final int score, final int leftNumberOfPins) {
        if (score < Roll.MIN_NUMBER_OF_PINS) {
            throw new IllegalArgumentException("Score should be greather than " + Roll.MIN_NUMBER_OF_PINS);
        }
        if (score > leftNumberOfPins) {
            throw new IllegalArgumentException("Score should be smaller than " + leftNumberOfPins);
        }
    }
}
