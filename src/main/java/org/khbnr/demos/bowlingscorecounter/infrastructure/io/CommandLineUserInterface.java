package org.khbnr.demos.bowlingscorecounter.infrastructure.io;

import static java.lang.System.in;
import static java.lang.System.out;

import java.util.Scanner;
import org.khbnr.demos.bowlingscorecounter.core.services.UserInterface;

public class CommandLineUserInterface implements UserInterface {

    private final Scanner scanner = new Scanner(in);

    @Override
    public int getScoreForRoll(final int frameNumber, final int rollNumber) {
        out.println(String.format("Please set score for frame %s - roll %s", frameNumber, rollNumber));
        return scanner.nextInt();
    }

    @Override
    public void printErrorMessage(final String errorMessage) {
        out.println(errorMessage);
    }

    @Override
    public void printResult(final int finalScore) {
        out.println(String.format("Game ended. Final Score is: %s", finalScore));
    }
}
