package org.khbnr.demos.bowlingscorecounter;

import org.khbnr.demos.bowlingscorecounter.core.services.BowlingScoreCounter;
import org.khbnr.demos.bowlingscorecounter.core.services.UserInterface;
import org.khbnr.demos.bowlingscorecounter.infrastructure.io.CommandLineUserInterface;

public class Application {

    private static final int DEFAULT_NUMBER_OF_FRAMES_PER_GAME = 10;

    public static void main(final String... args) {
        final UserInterface userInterface = new CommandLineUserInterface();
        final BowlingScoreCounter bowlingScoreCounter = new BowlingScoreCounter(userInterface, DEFAULT_NUMBER_OF_FRAMES_PER_GAME);
        final int finalScore = bowlingScoreCounter.playGame();
        userInterface.printResult(finalScore);
    }

}
