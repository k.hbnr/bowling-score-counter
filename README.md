# Description

The **bowling-score-counter** is an application to count the score of a standard bowling game applying rules for bonus points on strikes and spares. 

# Usage

To use the **bowling-score-counter**, clone the repo and run ```./gradlew build``` in the root directory of the repo. As a result, you will find a verified and built jar artifact under ```<REPO_ROOT>/build/libs```.
Invoke that artifact via ```java -jar <ARTIFACT_NAME>.jar``` to use the CommandLineUserInterface for the **bowling-score-counter**.


# Potential next steps

Pull Frame class out from BowlingScoreCounter class, probably as interface, and handle differences in applying bonus scores between regular and last frames as responsibility of this class.

